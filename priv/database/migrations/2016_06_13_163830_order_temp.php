<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderTemp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('orders_temp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_produk');
            $table->string('id_user');
            $table->string('total');
            $table->string('tanggal');
            $table->string('alamat');
            $table->string('telp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('orders_temp');
    }
}
