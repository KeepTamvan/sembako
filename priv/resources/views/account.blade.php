<!DOCTYPE html>
<html lang="en"><head>
    <title>Sembako | Akun</title>
    <meta charset="utf-8">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="ProFlex Powerful MultiPurpose Html Template">
    <meta name="author" content="ArtofThemes">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{!! csrf_token() !!}"/>

    <!-- Bootstrap -->
    <link rel="stylesheet" media="screen" href="css/bootstrap.min.css">
    
    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/color/red.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link rel="stylesheet" type="text/css" href="css/header-fullwidth.css">
    <link rel="stylesheet" type="text/css" href="css/tools.css">
    <link rel="stylesheet" type="text/css" href="css/footer-light.css">
    <link rel="stylesheet" type="text/css" href="css/socialmediaicons.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <link rel="stylesheet" type="text/css" href="css/animate.min.css">
    
    <!-- MegaMenu styles-->
    <link href="css/megamenu.css" rel="stylesheet">
    
    <!-- Font Awesome -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- LOAD JQUERY LIBRARY -->
	  <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    
    <!-- Favicons -->
		<link rel="shortcut icon" href="images/favicon.ico">
    
    <!-- Custom JS -->
    <script type="text/javascript" src="js/custom.js"></script>
    
    <!-- for Animation Elements -->
    <script src="js/wow.js"></script>

    <style type="text/css">
      .cek
      {
        margin-top: -100px;margin-left: 175px;
      }
    </style>

    <!-- SweetAlert -->
    {!! Html::style('css/sweetalert.css') !!}
    
  </head>
  <body>
  <!-- Header start -->
  	@include('header')
   <!-- Header end -->
   <!-- Main start -->
   <div class="main" role="main">
      	<!-- subheader start -->
      		<div class="row business-header">
      			<div class="container">

    			</div>
      		</div>
     	<!-- subheader end -->
      	<!-- Content start -->
      		<div class="row content">
      			<div class="container">
                <!-- Content Start -->
                    
                    <!-- Shop Start -->
                        
                    <div class="col-md-12 col-sm-12">
                    <h2 class="page-header">My Account </h2>                        

                          <p class="post-meta">
                          @if (Auth::check())
                            <span><i class="glyphicon glyphicon-eye-open"></i> Selamat Datang {{ Auth::user()->name }} </span>
                          </p>
                          
                          <div class="seperator-3d-dark top10 bottom20"></div>
                            
                            <!-- My Account Start -->
                            
                            <div class="row my-account padding-bottom30 padding-top30">
                                <div class="col-md-2">
                                  <ul class="nav nav-pills nav-stacked my-account-well">
                                    <li class="active"><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                                    <li><a href="{{ url('/history') }}"><i class="fa fa-shopping-cart"></i> History</a></li>
                                    <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out"></i> Logout</a></li>
                                  </ul>
                                </div>
                                <div class="col-md-10">
                                  <div class="my-account-panel"> <img class="my-account-pic img-circle" src="images/pft.jpg" alt="...">
                                    <div class="my-account-name"><small>{{ Auth::user()->name }}</small></div>
                                    <a href="#" class="btn btn-xs btn-primary pull-right" style="margin:10px;"><span class="glyphicon glyphicon-picture"></span> Change cover</a> </div>
                                  <br>
                                  <br>
                                  <br>
                                  <ul class="nav nav-tabs top20" id="myTab">
                                    <li class="active"><a href="#buah" data-toggle="tab"><i class="fa fa-shopping-cart"></i> Buah</a></li>
                                    <li><a href="#plastik" data-toggle="tab"><i class="fa fa-shopping-cart"></i> Plastik</a></li>
                                    <li><a href="#sayur" data-toggle="tab"><i class="fa fa-shopping-cart"></i> Sayur</a></li>
                                  </ul>
                                  <div class="tab-content">
                                    <div class="tab-pane" id="sayur">
                                      <!-- Tables start -->
                                      <div class="row">
                                        <div class="col-md-12">
                                            <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th>No</th>
                                                    <th>Nama</th>
                                                    <th width="10%">Quantity</th>
                                                    <th width="20%">Harga</th>
                                                    <th>Sub Total</th>
                                                  </tr>
                                                </thead>
                                                <tbody>                     
                                                <?php $i = 1; ?>
                                                @foreach ($sayur as $data)
                                                  <tr name="{{ $data->nama }}" id="{{ $data->id }}">
                                                  {!! Form::open(array('method' => 'POST', 'action' => 'HomeController@store', 'class' => 'form-horizontal')) !!}
                                                    <td>{{ $i++ }}</td>
                                                    <td>
                                                    {{ $data->nama }}
                                                    <input type="hidden" name="name[]" value="{{ $data->nama }}">
                                                    <input type="hidden" name="name_sayur" value="{{ $data->nama }}">
                                                    <input type="hidden" name="id[]" value="{{ $data->id }}">
                                                    </td>
                                                    <td class="sub-qty" jancuk = "suram-{{ $data->id }}">
                                                      <input type="number" name="qty_sayur" onchange="tambah()"  class="qty_sayur form-control" maxlength="100">
                                                    </td>
                                                    @if (Auth::user()->tipe == "a")
                                                      <td class="harga_sayur" rego = "{{ $data->tipe_a }}">
                                                      <input type="text" name="sayur_harga" id="bayar" rego="{{ $data->tipe_a }}" value="{{ $data->tipe_a }}" onchange="tambah(this)" class="form-control sayur_harga" readonly >
                                                      <input type="hidden" name="harga[]" id="bayar" rego="{{ $data->tipe_a }}" value="{{ $data->tipe_a }}" onchange="tambah(this)" class="form-control sayur_harga" readonly ></td>
                                                    @elseif (Auth::user()->tipe == "b")
                                                      <td name="harga_sayur"  rego = "{{ $data->tipe_b }}">
                                                      <input type="hidden" name="harga[]" id="bayar" rego="{{ $data->tipe_a }}" value="{{ $data->tipe_a }}" onchange="tambah(this)" class="form-control sayur_harga" readonly >
                                                      <input type="text" name="sayur_harga" id="bayar" onchange="tambah(this)" value="{{ $data->tipe_b }}" class="form-control sayur_harga" readonly ></td>
                                                    @else
                                                      <td name="harga_sayur"  rego = "{{ $data->tipe_c }}">
                                                      <input type="hidden" name="harga[]" id="bayar" rego="{{ $data->tipe_a }}" value="{{ $data->tipe_a }}" onchange="tambah(this)" class="form-control sayur_harga" readonly >
                                                      <input type="text" name="sayur_harga" id="bayar" onchange="tambah(this)" value="{{ $data->tipe_c }}" class="form-control sayur_harga" readonly ></td>
                                                    @endif
                                                    <td class='hasile'>
                                                    </td>
                                                  </tr>
                                                @endforeach
                                                </tbody>
                                              </table>
                                          </div>

                                                <div class="form-group">
                                                  <div class="col-md-2">
                                                    <input type="submit" data-loading-text="Loading..." class="btn btn-primary btn-sm" id="simpan" value="Order Sekarang">
                                                  </div>
                                                </div>
                                      </div>
                                      <!-- Tables start -->
                                      <?php $i++; ?>
                                    </div>

                                    <div class="tab-pane active" id="buah">
                                      <!-- Tables start -->
                                      <div class="row">
                                        <div class="col-md-12">
                                            <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th>No</th>
                                                    <th>Nama</th>
                                                    <th width="10%">Quantity</th>
                                                    <th width="20%">Harga</th>
                                                    <th>Sub Total</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                      <?php $i = 1; ?>
                                      @foreach ($buah as $data)
                                                  <tr name="{{ $data->nama }}" id="{{ $data->id }}">
                                                    <td>{{ $i++ }}</td>
                                                    <td>
                                                    {{ $data->nama }}
                                                    <input type="hidden" name="name[]" value="{{ $data->nama }}">
                                                    <input type="hidden" name="name_buah" value="{{ $data->nama }}">
                                                    <input type="hidden" name="id[]" value="{{ $data->id }}">
                                                    </td>
                                                    <td class="sub-qty" jancuk = "suram-{{ $data->id }}">
                                                    <input type="number" name="qty_buah" id="qty" onchange="tambah()" class="form-control" maxlength="100">
                                                    </td>
                                                    @if (Auth::user()->tipe == "a")
                                                      <td name="harga_sayur" rego = "{{ $data->tipe_a }}">
                                                      <input type="text" name="buah_harga" id="bayar" onchange="tambah(this)" value="{{ $data->tipe_a }}" class="form-control" readonly>
                                                      <input type="hidden" name="harga[]" id="bayar" onchange="tambah(this)" value="{{ $data->tipe_a }}" class="form-control" readonly></td>
                                                    @elseif (Auth::user()->tipe == "b")
                                                      <td name="harga_sayur" rego = "{{ $data->tipe_b }}">
                                                      <input type="text" name="buah_harga" id="bayar" onchange="tambah(this)" value="{{ $data->tipe_b }}" class="form-control" readonly><input type="hidden" name="harga[]" id="bayar" onchange="tambah(this)" value="{{ $data->tipe_a }}" class="form-control" readonly></td>
                                                    @else
                                                      <td name="harga_sayur" rego = "{{ $data->tipe_c }}">
                                                      <input type="text" name="buah_harga" id="bayar" onchange="tambah(this)" value="{{ $data->tipe_c }}" class="form-control" readonly><input type="hidden" name="harga[]" id="bayar" onchange="tambah(this)" value="{{ $data->tipe_a }}" class="form-control" readonly></td>
                                                    @endif
                                                    <td class='hasile'>
                                                    </td>
                                                  </tr>
                                      @endforeach
                                                </tbody>
                                              </table>
                                          </div>
                                                <div class="form-group">
                                                  <div class="col-md-2">
                                                    <input type="submit" data-loading-text="Loading..." class="btn btn-primary btn-sm" id="simpan" value="Order Sekarang">
                                                  </div>
                                                </div>
                                      </div>
                                      <!-- Tables start -->
                                    </div>

                                    <div class="tab-pane" id="plastik">
                                      <!-- Tables start -->
                                      <div class="row">
                                        <div class="col-md-12">
                                            <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th>No</th>
                                                    <th>Nama</th>
                                                    <th width="10%">Quantity</th>
                                                    <th width="20%">Harga</th>
                                                    <th>Sub Total</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr name="{{ $data->nama }}" id="{{ $data->id }}">
                                      <?php $i = 1; ?>
                                      @foreach ($plastik as $data)
                                                    <td>{{ $i++ }}</td>
                                                    <td>
                                                    {{ $data->nama }}
                                                    <input type="hidden" name="name[]" value="{{ $data->nama }}">
                                                    <input type="hidden" name="name_plastik" value="{{ $data->nama }}">
                                                    <input type="hidden" name="id[]" value="{{ $data->id }}">
                                                    </td>
                                                    <td class="sub-qty" jancuk = "suram-{{ $data->id }}">
                                                      <input type="number" name="qty_plastik" data-id="{{ $data->id }}" id="qty" onchange="tambah()" class="form-control" maxlength="100">
                                                    </td>
                                                    @if (Auth::user()->tipe == "a")
                                                      <td name="harga_sayur" rego = "{{ $data->tipe_a }}">
                                                      <input type="text" name="plastik_harga" id="bayar" value="{{ $data->tipe_a }}" class="form-control" readonly>
                                                      <input type="hidden" name="harga[]" id="bayar" value="{{ $data->tipe_a }}" class="form-control" readonly>
                                                      </td>
                                                    @elseif (Auth::user()->tipe == "b")
                                                      <td name="harga_sayur" rego = "{{ $data->tipe_b }}">
                                                      <input type="text" name="plastik_harga" id="bayar" value="{{ $data->tipe_b }}" class="form-control" readonly><input type="hidden" name="harga[]" id="bayar" value="{{ $data->tipe_a }}" class="form-control" readonly></td>
                                                    @else
                                                      <td name="harga_sayur" rego = "{{ $data->tipe_a }}">
                                                      <input type="text" name="plastik_harga" id="bayar" value="{{ $data->tipe_c }}" class="form-control" readonly ><input type="hidden" name="harga[]" id="bayar" value="{{ $data->tipe_a }}" class="form-control" readonly></td>
                                                    @endif
                                                    <td class='hasile'>
                                                    </td>
                                      @endforeach
                                                  </tr>
                                                </tbody>
                                              </table>
                                          </div>
                                          <div class="form-group">
                                              <div class="col-md-2">
                                                <input type="submit" data-loading-text="Loading..." class="btn btn-primary btn-sm" id="simpan" value="Order Sekarang">
                                              </div>
                                          </div>
                                      {!! Form::close() !!}
                                      </div>
                                      <!-- Tables start -->
                                    </div>

                                  </div>
                                </div>
                              </div>

                            @endif
                            <!-- My Account End -->
                            
                            <div class="seperator-3d-dark top30 bottom30"></div>
                            
                     </div>
                          
                    <!-- Shop End -->
                    
				<!-- Content End -->   
                
                </div>
      		</div>
     	<!-- Content end -->
 	</div>
    <!-- Main end -->

    @include('footer')


  	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster--> 
    <script src="js/bootstrap.min.js"></script>
    
    <!-- for Mega Menu -->
    <script src="js/for_megamenu_run_prettify.js"></script>
    
    <!-- for Retina Graphics -->
    <script type="text/javascript" src="js/retina.js"></script>
    @include('sweet::alert')
    @if (Session::has('sweet_alert.alert'))
    <script>
        swal({!! Session::get('sweet_alert.alert') !!});
    </script>
    @endif

    {!! Html::script('js/jquery.js') !!}

    <script type="text/javascript">
      function tambah()
      {
        // var qty = $('td.sub-qty').attr('jancuk');

        var ray = [];
        $.each($('.sub-qty'), function(i, item){
          var row  = {
                  id_barang : $(item).attr('jancuk'),
                  harga : $(item).next().attr('rego'),
                  qty : $(item).children().val(),
                  rego : $(item).next().attr('rego') * $(item).children().val()
          }
          ray.push(row);
          $('.sub-qty').next().next().html(ray[0].rego);
        });
        console.log(ray[0].rego);

      }
    </script>

    <script type="text/javascript">
    $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
    </script>

  </body>
</html>