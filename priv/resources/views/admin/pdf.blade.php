<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>Laporan Provinsi</title>
		<body>
			<style type="text/css">
				.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
				.tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
				.tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
				.tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
				.tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
				.tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
			</style>
 
			<div style="font-family:Arial; font-size:12px;">
				<center><h2>Laporan Order</h2></center>
			</div>
			<br>
			<table class="tg">
			  <tr>
			    <th class="tg-3wr7">#<br></th>
			    <th class="tg-3wr7">Nama<br></th>
			    <th class="tg-3wr7">Alamat<br></th>
			    <th class="tg-3wr7">Telp<br></th>
			    <th class="tg-3wr7">Sayur<br></th>
			    <th class="tg-3wr7">Buah<br></th>
			    <th class="tg-3wr7">Plastik<br></th>
			    <th class="tg-3wr8">Total Harga<br></th>
			  </tr>
			  @foreach ($order as $val)
			  <tr>
			    <td class="tg-rv4w" width="2%"><center>{{ ++$i }}</center></td>
			    <td class="tg-rv4w" width="10%">{{ $val->nama_pemesan }}</td>
			    <td class="tg-rv4w" width="10%">{{ $val->alamat }}</td>
			    <td class="tg-rv4w" width="10%">{{ $val->telp }}</td>
			    <td class="tg-rv4w" width="10%">{{ $val->sayur }} : {{ $val->qty_sayur }}</td>
			    <td class="tg-rv4w" width="10%">{{ $val->buah }} : {{ $val->qty_buah }}</td>
			    <td class="tg-rv4w" width="10%">{{ $val->plastik }} : {{ $val->qty_plastik }}</td>
			    <td class="tg-rv4w" width="10%">{{ $val->jumlah_dibayar }}</td>
			  </tr>
			  @endforeach
			</table>
		</body>
	</head>
</html>