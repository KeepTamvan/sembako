<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Aksa Admin | Order</title>

	<!-- Global stylesheets -->
	{!! Html::style('https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900') !!}
	{!! Html::style('assets/css/minified/bootstrap.min.css') !!}
	{!! Html::style('assets/css/icons/icomoon/styles.css') !!}
	{!! Html::style('assets/css/minified/core.min.css') !!}
	{!! Html::style('assets/css/minified/components.min.css') !!}
	{!! Html::style('assets/css/minified/colors.min.css') !!}
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	{!! Html::script('assets/plugins/loaders/pace.min.js') !!}
	{!! Html::script('assets/core/libraries/jquery.min.js') !!}
	{!! Html::script('assets/core/libraries/bootstrap.min.js') !!}
	{!! Html::script('assets/plugins/loaders/blockui.min.js') !!}
	{!! Html::script('assets/plugins/ui/nicescroll.min.js') !!}
	{!! Html::script('assets/plugins/ui/drilldown.js') !!}
	<!-- /core JS files -->

	<!-- Theme JS files -->
	{!! Html::script('assets/plugins/tables/datatables/datatables.min.js') !!}
	{!! Html::script('assets/plugins/forms/selects/select2.min.js') !!}

	{!! Html::script('assets/core/app.js') !!}
	{!! Html::script('assets/pages/datatables_basic.js') !!}
	<!-- /theme JS files -->

	<!-- Sweet Alert -->
    {!! Html::style('css/sweetalert.css') !!}
    {!! Html::script('js/sweetalert.min.js') !!}

    @include('sweet::alert')
    @if (Session::has('sweet_alert.alert'))
    <script>
        swal({!! Session::get('sweet_alert.alert') !!});
    </script>
    @endif

</head>

<body>

	<!-- Main navbar -->
	@include('admin.navbar')
	<!-- /main navbar -->


	<!-- Second navbar -->
	@include('admin.snavbar')
	<!-- /second navbar -->

	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Aksa Admin</span> - Order</h4>

				<ul class="breadcrumb breadcrumb-caret position-right">
					<li><a href="{{ url('/aksa-admin/home') }}">Home</a></li>
					<li class="active"><a href="{{ url('/aksa-admin/order') }}">Order</a></li>
				</ul>
			</div>

		</div>
	</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

			<div class="col-md-12">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h6 class="panel-title">List Order Data</h6>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>

							<div class="panel-body">
								<div class="tabbable">
									<ul class="nav nav-tabs nav-tabs-highlight">
										<li class="active"><a href="#css-animate-tab1" data-toggle="tab">Harian</a></li>
										<li><a href="#css-animate-tab2" data-toggle="tab">Mingguan</a></li>
										<li><a href="#css-animate-tab3" data-toggle="tab">Bulanan</a></li>
									</ul>

									<div class="tab-content">
										<div class="tab-pane animated bounceIn active" id="css-animate-tab1">
											<div class="panel panel-flat">

									<div class="panel-heading">
										<h5 class="panel-title">Data Order Harian</h5>
										<div class="heading-elements">
											<ul class="icons-list">
											<a href="{{ url('/aksa-admin/order/pdf') }}" type="button" class="btn btn-primary"><i class="icon-cog3 position-left"></i> Export As PDF</a>
						                		<li></li>
						                		<li><a data-action="reload"></a></li>
						                		<li><a data-action="close"></a></li>
						                	</ul>
					                	</div>
									</div>

									<table class="table datatable-basic table-bordered table-striped table-hover">

										<thead>
											<tr>
												<th>No</th>
												<th>Nama</th>
												<th>Email</th>
												<th>Alamat</th>
												<th>Telp</th>
												<th>Total Dibayar</th>
												<th>Status</th>
												<th class="text-center">Aksi</th>
											</tr>
										</thead>

										<tbody>
											@foreach ($order as $data)
											<tr>
												<td>{{ ++$no }}</td>
												<td>{{ $data->nama_pemesan }}</td>
												<td>{{ $data->email }}</td>
												<td>{{ $data->alamat }}</td>
												<td>{{ $data->telp }}</td>
												<td>{{ $data->jumlah_dibayar }}</td>
												<td>{{ $data->status }}</td>
												<td class="text-center">
													<ul class="icons-list">
														<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown">
																<i class="icon-menu9"></i>
															</a>

															<ul class="dropdown-menu dropdown-menu-right">
																<li><a href="#" data-toggle="modal" data-target="#modal_detail_primary{{{ $data->id_orders }}}"><i class="icon-eye"></i> Detail</a></li>
																<li><a href="#" data-toggle="modal" data-target="#modal_theme_primary{{{ $data->id_orders }}}"><i class="icon-trash"></i> Delete</a></li>
															</ul>
														</li>
													</ul>
												</td>
											</tr>

											<!-- Hapus modal -->
											<div id="modal_theme_primary{{{ $data->id_orders }}}" class="modal fade">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header bg-danger">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
															<h6 class="modal-title">Hapus Produk</h6>
														</div>

														<div class="modal-body">
															<h6 class="text-semibold">Apakah Anda Yakin Ingin Menghapus <i> {{ $data->nama_pemesan }} </i></h6>
															<p>NB : Data tidak dapat dikembalikan jika sudah dihapus.</p>
														</div>

														<div class="modal-footer">
															{!! Form::open(array('method' => 'DELETE', 'route' => array('aksa-admin.order.destroy', $data->id))) !!}
						                                      {!! Form::submit("Ya", array('class' => 'btn btn-danger')) !!}
						                                    {!! Form::close() !!}
						                                    <button type="button" class="btn btn-link" data-dismiss="modal">Tidak</button>
														</div>
													</div>
												</div>
											</div>
											<!-- /Hapus modal -->

											<!-- Detail modal -->
											<div id="modal_detail_primary{{{ $data->id_orders }}}" class="modal fade">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header bg-danger">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
															<h6 class="modal-title">Detail Order</h6>
														</div>

														<div class="modal-body">
															<h3 class="text-semibold">Identitas </h3>
															<h6 class="text-semibold">Nama Pemesan : {{ $data->nama_pemesan }} </h6>
															<h6 class="text-semibold">Alamat : {{ $data->alamat }} </h6>
															<h6 class="text-semibold">No Telp : {{ $data->telp }} </h6>
															<hr />
															<h3 class="text-semibold">List Orderan </h3>
															<h6 class="text-semibold">{{ $data->sayur }} : {{ $data->qty_sayur }} </h6>
															<h6 class="text-semibold">{{ $data->buah }} : {{ $data->qty_buah }} </h6>
															<h6 class="text-semibold">{{ $data->plastik }} : {{ $data->qty_plastik }} </h6>
															<hr />
															<h3 class="text-semibold">Jumlah Total </h3>
															<h6 class="text-semibold">{{ $data->buah }} : {{ $data->jumlah_dibayar }}</h6>
														</div>

														<div class="modal-footer">
						                                      {!! Form::submit("Close", array('class' => 'btn btn-link', 'data-dismiss' => 'modal')) !!}
														</div>
													</div>
												</div>
											</div>
											<!-- /Detail modal -->
											@endforeach
											
										</tbody>
									</table>
								</div>
										</div>

										<div class="tab-pane animated fadeInUp" id="css-animate-tab2">
													<div class="panel panel-flat">

									<div class="panel-heading">
										<h5 class="panel-title">Data Order Mingguan</h5>
										<div class="heading-elements">
											<ul class="icons-list">
											<a href="{{ url('/aksa-admin/order/pdf') }}" type="button" class="btn btn-primary"><i class="icon-cog3 position-left"></i> Export As PDF</a>
						                		<li></li>
						                		<li><a data-action="reload"></a></li>
						                		<li><a data-action="close"></a></li>
						                	</ul>
					                	</div>
									</div>

									<table class="table datatable-basic table-bordered table-striped table-hover">

										<thead>
											<tr>
												<th>No</th>
												<th>Nama</th>
												<th>Email</th>
												<th>Alamat</th>
												<th>Telp</th>
												<th>Total Dibayar</th>
												<th>Status</th>
												<th class="text-center">Aksi</th>
											</tr>
										</thead>

										<tbody>
											@foreach ($mg as $data)
											<tr>
												<td>{{ ++$no }}</td>
												<td>{{ $data->nama_pemesan }}</td>
												<td>{{ $data->email }}</td>
												<td>{{ $data->alamat }}</td>
												<td>{{ $data->telp }}</td>
												<td>{{ $data->jumlah_dibayar }}</td>
												<td>{{ $data->status }}</td>
												<td class="text-center">
													<ul class="icons-list">
														<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown">
																<i class="icon-menu9"></i>
															</a>

															<ul class="dropdown-menu dropdown-menu-right">
																<li><a href="#" data-toggle="modal" data-target="#modal_detail_primary{{{ $data->id_orders }}}"><i class="icon-eye"></i> Detail</a></li>
																<li><a href="#" data-toggle="modal" data-target="#modal_theme_primary{{{ $data->id_orders }}}"><i class="icon-trash"></i> Delete</a></li>
															</ul>
														</li>
													</ul>
												</td>
											</tr>

											<!-- Hapus modal -->
											<div id="modal_theme_primary{{{ $data->id_orders }}}" class="modal fade">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header bg-danger">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
															<h6 class="modal-title">Hapus Produk</h6>
														</div>

														<div class="modal-body">
															<h6 class="text-semibold">Apakah Anda Yakin Ingin Menghapus <i> {{ $data->nama_pemesan }} </i></h6>
															<p>NB : Data tidak dapat dikembalikan jika sudah dihapus.</p>
														</div>

														<div class="modal-footer">
															{!! Form::open(array('method' => 'DELETE', 'route' => array('aksa-admin.order.destroy', $data->id))) !!}
						                                      {!! Form::submit("Ya", array('class' => 'btn btn-danger')) !!}
						                                    {!! Form::close() !!}
						                                    <button type="button" class="btn btn-link" data-dismiss="modal">Tidak</button>
														</div>
													</div>
												</div>
											</div>
											<!-- /Hapus modal -->

											<!-- Detail modal -->
											<div id="modal_detail_primary{{{ $data->id_orders }}}" class="modal fade">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header bg-danger">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
															<h6 class="modal-title">Detail Order</h6>
														</div>

														<div class="modal-body">
															<h3 class="text-semibold">Identitas </h3>
															<h6 class="text-semibold">Nama Pemesan : {{ $data->nama_pemesan }} </h6>
															<h6 class="text-semibold">Alamat : {{ $data->alamat }} </h6>
															<h6 class="text-semibold">No Telp : {{ $data->telp }} </h6>
															<hr />
															<h3 class="text-semibold">List Orderan </h3>
															<h6 class="text-semibold">{{ $data->sayur }} : {{ $data->qty_sayur }} </h6>
															<h6 class="text-semibold">{{ $data->buah }} : {{ $data->qty_buah }} </h6>
															<h6 class="text-semibold">{{ $data->plastik }} : {{ $data->qty_plastik }} </h6>
															<hr />
															<h3 class="text-semibold">Jumlah Total </h3>
															<h6 class="text-semibold">{{ $data->buah }} : {{ $data->jumlah_dibayar }}</h6>
														</div>

														<div class="modal-footer">
						                                      {!! Form::submit("Close", array('class' => 'btn btn-link', 'data-dismiss' => 'modal')) !!}
														</div>
													</div>
												</div>
											</div>
											<!-- /Detail modal -->
											@endforeach
											
										</tbody>
									</table>
								</div>
										</div>

										<div class="tab-pane animated zoomIn" id="css-animate-tab3">
													<div class="panel panel-flat">

									<div class="panel-heading">
										<h5 class="panel-title">Data Order Bulanan</h5>
										<div class="heading-elements">
											<ul class="icons-list">
											<a href="{{ url('/aksa-admin/order/pdf') }}" type="button" class="btn btn-primary"><i class="icon-cog3 position-left"></i> Export As PDF</a>
						                		<li></li>
						                		<li><a data-action="reload"></a></li>
						                		<li><a data-action="close"></a></li>
						                	</ul>
					                	</div>
									</div>

									<table class="table datatable-basic table-bordered table-striped table-hover">

										<thead>
											<tr>
												<th>No</th>
												<th>Nama</th>
												<th>Email</th>
												<th>Alamat</th>
												<th>Telp</th>
												<th>Total Dibayar</th>
												<th>Status</th>
												<th class="text-center">Aksi</th>
											</tr>
										</thead>

										<tbody>
											@foreach ($bulan as $data)
											<tr>
												<td>{{ ++$no }}</td>
												<td>{{ $data->nama_pemesan }}</td>
												<td>{{ $data->email }}</td>
												<td>{{ $data->alamat }}</td>
												<td>{{ $data->telp }}</td>
												<td>{{ $data->jumlah_dibayar }}</td>
												<td>{{ $data->status }}</td>
												<td class="text-center">
													<ul class="icons-list">
														<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown">
																<i class="icon-menu9"></i>
															</a>

															<ul class="dropdown-menu dropdown-menu-right">
																<li><a href="#" data-toggle="modal" data-target="#modal_detail_primary{{{ $data->id_orders }}}"><i class="icon-eye"></i> Detail</a></li>
																<li><a href="#" data-toggle="modal" data-target="#modal_theme_primary{{{ $data->id_orders }}}"><i class="icon-trash"></i> Delete</a></li>
															</ul>
														</li>
													</ul>
												</td>
											</tr>

											<!-- Hapus modal -->
											<div id="modal_theme_primary{{{ $data->id_orders }}}" class="modal fade">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header bg-danger">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
															<h6 class="modal-title">Hapus Produk</h6>
														</div>

														<div class="modal-body">
															<h6 class="text-semibold">Apakah Anda Yakin Ingin Menghapus <i> {{ $data->nama_pemesan }} </i></h6>
															<p>NB : Data tidak dapat dikembalikan jika sudah dihapus.</p>
														</div>

														<div class="modal-footer">
															{!! Form::open(array('method' => 'DELETE', 'route' => array('aksa-admin.order.destroy', $data->id))) !!}
						                                      {!! Form::submit("Ya", array('class' => 'btn btn-danger')) !!}
						                                    {!! Form::close() !!}
						                                    <button type="button" class="btn btn-link" data-dismiss="modal">Tidak</button>
														</div>
													</div>
												</div>
											</div>
											<!-- /Hapus modal -->

											<!-- Detail modal -->
											<div id="modal_detail_primary{{{ $data->id_orders }}}" class="modal fade">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header bg-danger">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
															<h6 class="modal-title">Detail Order</h6>
														</div>

														<div class="modal-body">
															<h3 class="text-semibold">Identitas </h3>
															<h6 class="text-semibold">Nama Pemesan : {{ $data->nama_pemesan }} </h6>
															<h6 class="text-semibold">Alamat : {{ $data->alamat }} </h6>
															<h6 class="text-semibold">No Telp : {{ $data->telp }} </h6>
															<hr />
															<h3 class="text-semibold">List Orderan </h3>
															<h6 class="text-semibold">{{ $data->sayur }} : {{ $data->qty_sayur }} </h6>
															<h6 class="text-semibold">{{ $data->buah }} : {{ $data->qty_buah }} </h6>
															<h6 class="text-semibold">{{ $data->plastik }} : {{ $data->qty_plastik }} </h6>
															<hr />
															<h3 class="text-semibold">Jumlah Total </h3>
															<h6 class="text-semibold">{{ $data->buah }} : {{ $data->jumlah_dibayar }}</h6>
														</div>

														<div class="modal-footer">
						                                      {!! Form::submit("Close", array('class' => 'btn btn-link', 'data-dismiss' => 'modal')) !!}
														</div>
													</div>
												</div>
											</div>
											<!-- /Detail modal -->
											@endforeach
											
										</tbody>
									</table>
								</div>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				<!-- /animations -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->


		<!-- Footer -->
		@include('admin.footer')
		<!-- /footer -->

	</div>
	<!-- /page container -->

</body>
</html>