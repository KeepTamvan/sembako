<!DOCTYPE html>
<html lang="en"><head>
    <title>Sembako | Akun</title>
    <meta charset="utf-8">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="ProFlex Powerful MultiPurpose Html Template">
    <meta name="author" content="ArtofThemes">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link rel="stylesheet" media="screen" href="css/bootstrap.min.css">
    
    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/color/red.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link rel="stylesheet" type="text/css" href="css/header-fullwidth.css">
    <link rel="stylesheet" type="text/css" href="css/tools.css">
    <link rel="stylesheet" type="text/css" href="css/footer-light.css">
    <link rel="stylesheet" type="text/css" href="css/socialmediaicons.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <link rel="stylesheet" type="text/css" href="css/animate.min.css">
    
    <!-- MegaMenu styles-->
    <link href="css/megamenu.css" rel="stylesheet">
    
    <!-- Font Awesome -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

        <!--[if lte IE 7]><script src="js/socialmediaicons-lte-ie7.js"></script><![endif]-->

    <!-- LOAD JQUERY LIBRARY -->
	<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    
    <!-- Favicons -->
		<link rel="shortcut icon" href="images/favicon.ico">
    
    <!-- Custom JS -->
    <script type="text/javascript" src="js/custom.js"></script>
    
  <!-- for Animation Elements -->
    <script src="js/wow.js"></script>

    <style type="text/css">
      .cek
      {
        margin-top: -100px;margin-left: 175px;
      }
    </style>
    
  </head>
  <body>
  <!-- Header start -->
  	@include('header')
   <!-- Header end -->
   <!-- Main start -->
   <div class="main" role="main">
      	<!-- subheader start -->
      		<div class="row business-header">
      			<div class="container">

    			</div>
      		</div>
     	<!-- subheader end -->
      	<!-- Content start -->
      		<div class="row content">
      			<div class="container">
                <!-- Content Start -->
                    
                    <!-- Shop Start -->
                        
                    <div class="col-md-12 col-sm-12">
                    <h2 class="page-header">My Account </h2>                        

                          <p class="post-meta">
                          @if (Auth::check())
                            <span><i class="glyphicon glyphicon-eye-open"></i> Selamat Datang {{ Auth::user()->name }} </span>
                          </p>
                          
                          <div class="seperator-3d-dark top10 bottom20"></div>
                            
                            <!-- My Account Start -->
                            
                            <div class="row my-account padding-bottom30 padding-top30">
                                <div class="col-md-2">
                                  <ul class="nav nav-pills nav-stacked my-account-well">
                                    <li><a href="{{ url('/akun') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                    <li class="active"><a href="{{ url('/history') }}"><i class="fa fa-shopping-cart"></i> History</a></li>
                                    <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out"></i> Logout</a></li>
                                  </ul>
                                </div>
                                <div class="col-md-10">
                                  <div class="my-account-panel"> <img class="my-account-pic img-circle" src="images/pft.jpg" alt="...">
                                    <div class="my-account-name"><small>{{ Auth::user()->name }}</small></div>
                                    <a href="#" class="btn btn-xs btn-primary pull-right" style="margin:10px;"><span class="glyphicon glyphicon-picture"></span> Change cover</a> </div>
                                  <br>
                                  <br>
                                  <br>

                                    <div class="tab-pane" id="pesanan">
                                    <?php $i = 1; ?>
                                      <!-- Tables start -->
                                      <div class="row">
                                      <h4>List Orderan</h4>
                                        <div class="col-md-12">
                                            <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th>No</th>
                                                    <th>Pemesan</th>
                                                    <th>Pesanan</th>
                                                    <th>Tanggal</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                      @foreach ($pft as $data)
                                                  <tr>
                                                    <td>
                                                    {{ $i }}
                                                    </td>
                                                    <td>
                                                    {{ $data->name }}
                                                    </td>
                                                    <td>
                                                    {{ $data->nama }} : {{ $data->total_qty }}
                                                    </td>
                                                    <td>
                                                    {{ $data->created_at }}
                                                    </td>
                                                  </tr>
                                      <?php $i++; ?>
                                      @endforeach
                                                </tbody>
                                              </table>
                                          </div>
                                      </div>
                                      <!-- Tables start -->
                                    </div>

                                  </div>
                                </div>
                              </div>
                            @endif
                            <!-- My Account End -->
                            
                            <div class="seperator-3d-dark top30 bottom30"></div>
                            
                     </div>
                          
                    <!-- Shop End -->
                    
				<!-- Content End -->   
                
                </div>
      		</div>
     	<!-- Content end -->
 	</div>
    <!-- Main end -->

    @include('footer')


  	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster--> 
    <script src="js/bootstrap.min.js"></script>
    
    <!-- for Mega Menu -->
    <script src="js/for_megamenu_run_prettify.js"></script>
    
    <!-- for Retina Graphics -->
    <script type="text/javascript" src="js/retina.js"></script>
    @include('sweet::alert')
    @if (Session::has('sweet_alert.alert'))
    <script>
        swal({!! Session::get('sweet_alert.alert') !!});
    </script>
    @endif

    {!! Html::script('js/jquery.js') !!}

    <script type="text/javascript">
      function tambah(){

        var nilai1=document.getElementById("qty").value;
        var nilai2=document.getElementById("bayar").value;
        var tambah=parseInt(nilai1)*parseInt(nilai2);
        
        document.getElementById("Tbayar").value=parseInt(tambah);
      }
    </script>

  </body>
</html>