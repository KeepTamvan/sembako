<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //
    protected $table = "Orders_temp";

    public function Order()
    {
    	return $this->belongsToMany('orders', 'id_orders', 'id_kustomer', 'kode_orders');
    }

    public $timestamps = false;
    
}
