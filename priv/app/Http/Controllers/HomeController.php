<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Order;
use App\Catalog;
use App\Cart;
use App\Menu;
use Illuminate\Support\Facades\Input;
use Validator;
use Alert;
use Auth;
use Mail;
use Carbon\Carbon;
use DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // code ...
        $data = Catalog::orderBy('id', 'asc')->take(6)->get();
        return view('index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // code ...
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // code ...
        $validator = Validator::make(
            Input::all(), array(
                'name' => 'required'
                )
            );

        $qty_buah = $request->input('qty_buah');
        $qty_sayur = $request->input('qty_sayur');
        $qty_plastik  = $request->input('qty_plastik');
        $harga_sayur = $request->input('sayur_harga');
        $harga_buah = $request->input('buah_harga');
        $harga_plastik = $request->input('plastik_harga');
        $pesanan = Input::get('name');

        $total_qty = $qty_plastik + $qty_sayur + $qty_buah;

        $sub_buah = $qty_buah * $harga_buah;
        $sub_sayur = $qty_sayur * $harga_sayur;
        $sub_plastik = $qty_plastik * $harga_plastik;

        $jum_total = $sub_plastik + $sub_sayur + $sub_buah;
        $date = Carbon::today();

        if ($validator->passes()) {
            # code...
            $order = new Order();
            $order->name = Auth::user()->name;
            $order->alamat = Auth::user()->alamat;
            $order->telp = Auth::user()->telp;
            $order->id_kustomer = Auth::user()->id;
            $order->kode_orders = str_random();
            $order->created_at = $date;
            $order->updated_at = $date;
            $order->save();

            $ids = $request->input('id');
            foreach ($ids as $key => $id) {
                # code...
                $cok = new Cart();
                $cok->id_produk = $id;
                $cok->id_order = $order->id;
                $cok->qty_plastik = $qty_plastik;
                $cok->qty_sayur = $qty_sayur;
                $cok->qty_buah = $qty_buah;
                $cok->total_qty = $total_qty;
                $cok->harga_s = $sub_sayur;
                $cok->harga_b = $sub_buah;
                $cok->harga_p = $sub_plastik;
                $cok->dibayar = $jum_total;
                $cok->pesenan = $pesanan[$key];
                $cok->save();
            }

            $data = array(
                'name' => Auth::user()->name,
                'lamat' => Auth::user()->alamat,
                'telp' => Auth::user()->telp,
                'dateo' => $date,
                'kode' => $order->kode_orders,
                'dibayar' => $jum_total,
                'pesanan' => Input::get('name'),
                'harga' => Input::get('harga'),
                'qty' => $cok->qty,
                );

            Mail::send('mail', $data, function ($message) use ($data)
            {
                $message->to('outattacker@gmail.com', 'Administrator')->subject('Order dari Sembako.com');
            });

            Alert::success(" Order Berhasil ", "Success");
            return redirect('/akun');
        } else {
            alert()->error(" Gagal Order , Coba Lagi", "Error");
            return redirect('/akun');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $no = 0;
        $data = Catalog::orderBy('id', 'desc')->where('kat', '=', 'sayur')->get();
        $buah = Catalog::orderBy('id', 'desc')->where('kat', '=', 'buah')->get();
        $plastik = Catalog::orderBy('id', 'desc')->where('kat', '=', 'plastik')->get();
        $menu = Menu::orderBy('id', 'desc')->get();

        return view('account')->with('sayur', $data)->with('buah', $buah)->with('plastik', $plastik)->with('no', $no)->with('menu', $menu);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showProduk()
    {
        $data = Catalog::orderBy('id', 'asc')->paginate(10);
        return view('shop')->with('data', $data);
    }

    public function showPesanan()
    {
        $orderan = DB::table('orders')
                    ->join('orders_temp', 'orders.id', '=', 'orders_temp.id_order')
                    ->join('kategori', 'orders_temp.id_produk', '=', 'kategori.id')
                    ->select('orders.name','kategori.nama','orders_temp.total_qty', 'orders.created_at')
                    ->where('orders.id_kustomer', '=', Auth::user()->id)
                    ->where('orders.created_at', '=', Carbon::today())
                    ->take(4)
                    ->get();

        return view('history')->with('pft', $orderan);
    }

}
