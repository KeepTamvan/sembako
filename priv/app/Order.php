<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table = "orders";

    public function Cart()
    {
    	return $this->belongsToMany('orders_temp', 'id_produk', 'id_user');
    }
}
